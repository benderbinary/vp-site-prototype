import React from "react"
import { Link } from "gatsby"
import Hero from "../components/hero"
import Details from "../components/details"
import Cta from "../components/cta"
import Footer from "../components/footer"

import "../css/framework.css"
import "../css/style.css"
// import SEO from "../components/seo"

const IndexPage = () => (
  <>
    <Hero />
    <Details />
    <Cta />
    <Footer />
  </>
)

export default IndexPage
