import React from 'react';
import Image from 'gatsby-image';
import { graphql } from 'gatsby';
import { useStaticQuery } from 'gatsby';
import BackgroundImage from 'gatsby-background-image';

const query = graphql`
	{
		strapiHero {
			id
			title
			subtitle
			image {
				childImageSharp {
					fluid(quality: 90, maxWidth: 2000) {
						...GatsbyImageSharpFluid
					}
				}
			}
			logo {
				childImageSharp {
					fixed(quality: 90, width: 106, height: 47) {
						...GatsbyImageSharpFixed
					}
				}
			}
		},
		file(relativePath: {eq: "blue-gradient-coming-soon-bg-scaled.jpg"}) {
			id
			childImageSharp {
				fluid {
					...GatsbyImageSharpFluid
				}
			}
		}
	}
`;

const Hero = () => {
	const data = useStaticQuery(query);
	console.log(data);

	return (
		<section className="color-white text-center bg-dark color-filter-dark-3">
			<BackgroundImage className="pt-100" fluid={data.file.childImageSharp.fluid}>
				<div className="container px-xl-0">
					<div className="row justify-content-center">
						<div className="hero-intro col-lg-10">
							<Image
								fixed={data.strapiHero.logo.childImageSharp.fixed}
								alt=""
								className="mt-60 img-fluid"
								data-aos-duration="900"
								data-aos="fade-up"
								data-aos-delay="450"
							/>
							<h3
								className="prefix mt-60"
								data-aos-duration="900"
								data-aos="fade-in"
								data-aos-delay="250"
							>
								Welcome to
							</h3>
							<h2 className="mb-10" data-aos-duration="900" data-aos="fade-in" data-aos-delay="500">
								{data.strapiHero.title}
							</h2>
						</div>
						<div className="col-sm-11 col-md-9 col-lg-7 col-xl-5">
							<div className="mt-15 f-22 medium op-7 text-adaptive">
								<div data-aos-duration="900" data-aos="fade-in" data-aos-delay="750">
									{data.strapiHero.subtitle}
								</div>
							</div>
						</div>
						<div className="col-12">
							<Image
								fluid={data.strapiHero.image.childImageSharp.fluid}
								alt=""
								className="mt-60 img-fluid"
								data-aos-duration="900"
								data-aos="fade-up"
								data-aos-delay="450"
							/>
						</div>
					</div>
				</div>
			</BackgroundImage>
		</section>
	);
};

export default Hero;
