import React from 'react';
import Image from 'gatsby-image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useStaticQuery } from 'gatsby';
import { faPlay } from '@fortawesome/free-solid-svg-icons';

const query = graphql`
	{
		strapiDetails {
			id
			title
			video_url
			content
			button
			video_overlay_image {
				childImageSharp {
					fluid(quality: 90) {
						...GatsbyImageSharpFluid
					}
				}
			}
		}
	}
`;

const Details = () => {
	const data = useStaticQuery(query);
	console.log(data);
	return (
		<section className="pt-100 pb-100 bg-light content_26">
			<div className="container px-xl-0">
				<div className="row justify-content-center">
					<div className="col-xl-10 col-lg-11">
						<div className="row flex-row-reverse justify-content-between">
							<div className="col-md-7 mb-3 mb-md-0">
								<div
									className="relative poster"
									data-aos-duration="900"
									data-aos="fade-down"
									data-aos-delay="450"
								>
									<Image
										fluid={data.strapiDetails.video_overlay_image.childImageSharp.fluid}
										alt=""
										classNameName="mt-60 img-fluid"
										data-aos-duration="900"
										data-aos="fade-up"
										data-aos-delay="450"
									/>
									<a
										href={data.strapiDetails.video_url}
										className="play action-2"
										data-fancybox="content_26_fancybox"
									>
										<FontAwesomeIcon icon={faPlay} size="1x" />
									</a>
								</div>
							</div>
							<div className="col-md-4">
								<div
									className="mt-15 inner"
									data-aos-duration="900"
									data-aos="fade-down"
									data-aos-delay="0"
								>
									<h2 className="f-22">{data.strapiDetails.title}</h2>
									<div className="mt-15 color-heading text-adaptive">
										{data.strapiDetails.content}{' '}
									</div>
									<a
										className="btn mt-30 sm border-gray color-main f-16"
										href="https://www.virginpulse.com/schedule-your-demo-of-virgin-pulses-industry-leading-software?utm_source=website&#038;utm_medium=solution-page&#038;utm_campaign=CAM-2020-Request-a-Demo"
										target="_self"
									>
										{data.strapiDetails.button}
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Details;
