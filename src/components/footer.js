import React from 'react';

const Footer = () => {
	return (
		<footer className="pt-70 pb-65 bg-light text-center footer_2">
			<div className="container px-xl-0">
				<div className="row justify-content-center">
					<div className="col-12 lh-40" data-aos-duration="900" data-aos="fade-down" data-aos-delay="0">
						<a href="https://www.virginpulse.com/our-solutions/" className="link color-main mx-20">
							Our Solutions
						</a>

						<a href="https://www.virginpulse.com/about-us/" className="link color-main mx-20">
							About Us
						</a>

						<a href="https://www.virginpulse.com/contact/" className="link color-main mx-20">
							Contact Us
						</a>
					</div>
					<div className="mt-20 col-12" data-aos-duration="900" data-aos="fade-down" data-aos-delay="450">
						<div className="color-heading text-adaptive">
							<p>
								Copyright Virgin Pulse 2020. All Rights Reserved.{' '}
								<a
									href="https://www.virginpulse.com/user-terms-and-conditions/"
									rel="noopener"
									target="_blank"
								>
									Terms &#038; Conditions
								</a>{' '}
								and{' '}
								<a href="https://www.virginpulse.com/privacy-notice/" rel="noopener" target="_blank">
									Privacy Notice
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	);
};

export default Footer;
