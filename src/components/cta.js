import React from 'react';
import { useStaticQuery } from 'gatsby';
import BackgroundImage from 'gatsby-background-image';

const query = graphql`
	{
		strapiCallToAction {
			id
			title
			subtitle
			button
		}
		file(relativePath: { eq: "cta-footer-bg@2x.png" }) {
			id
			childImageSharp {
				fluid {
					...GatsbyImageSharpFluid
				}
			}
		}
	}
`;

const Cta = () => {
	const data = useStaticQuery(query);
	console.log(data);
	return (
		<section className="bg-dark text-center color-white call_to_action_4">
			<BackgroundImage className="pt-105 pb-100" fluid={data.file.childImageSharp.fluid}>
				<div className="container px-xl-0">
					<div className="row justify-content-center">
						<div className="col-xl-10">
							<h2 className="" data-aos-duration="900" data-aos="fade-down" data-aos-delay="0">
								{data.strapiCallToAction.title}
							</h2>
						</div>
						<div className="col-xl-6 col-lg-8 col-md-9 col-sm-10">
							<div
								className="mt-25 f-22 text-adaptive"
								data-aos-duration="900"
								data-aos="fade-down"
								data-aos-delay="450"
							>
								{data.strapiCallToAction.subtitle}{' '}
							</div>
							<div data-aos-duration="900" data-aos="fade-down" data-aos-delay="900">
								<a
									className="btn mt-50 lg action-2"
									href="https://www.virginpulse.com/schedule-your-demo-of-virgin-pulses-industry-leading-software?utm_source=website&#038;utm_medium=solution-page&#038;utm_campaign=CAM-2020-Request-a-Demo"
									target="_self"
								>
									{data.strapiCallToAction.button}
								</a>
							</div>
						</div>
					</div>
				</div>
			</BackgroundImage>
		</section>
	);
};

export default Cta;
